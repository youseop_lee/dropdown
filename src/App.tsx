import './App.scss';

import React, { useState, useCallback, WheelEvent } from 'react';

import Landing from './Landing';
import Components from './Components';

function App() {
  const [page, setPage] = useState(0);

  const style = {
    transform: `translateY(${page * -100}%)`,
    transition: `transform 700ms ease-in-out`,
  }

  const onWheel = useCallback(
    (e: WheelEvent) => {
      e.stopPropagation();

      if (e.deltaY > 0 && page !== 1) {
        setPage(page + 1);
      }
      else if (e.deltaY < 0 && page !== 0) {
        setPage(page - 1);
      }
    }, [page]
  );

  return (
    <div className="app-body" style={style} onWheel={onWheel}>
      <Landing />
      <Components />
    </div>
  );
}

export default App;
