import Dropdown from './components/dropdown/Dropdown';
import DropdownItem from './components/dropdown/DropdownItem';
import DropdownSubmenu from './components/dropdown/DropdownSubmenu';
import DropdownDivider from './components/dropdown/DropdownDivider';
import { useState } from 'react';

const SimpleDropdown = () => {
  const [currentLabel, setLabel] = useState('Select one');

  const onClick = (label: string) => {
    setLabel(label);
  }

  return (
    <Dropdown label={currentLabel}>
      <DropdownItem label={"Item 1"} onClick={onClick}/>
      <DropdownItem label={"Item 2"} onClick={onClick}>
        <DropdownSubmenu>
          <DropdownItem label={"sub 1"} onClick={onClick}/>
          <DropdownItem label={"sub 2"} onClick={onClick}>
            <DropdownSubmenu>
              <DropdownItem label={"sub sub 1"} onClick={onClick}/>
              <DropdownItem label={"sub sub 2"} onClick={onClick}/>
            </DropdownSubmenu>
          </DropdownItem>
        </DropdownSubmenu>
      </DropdownItem>
      <DropdownItem label={"Item 3"} onClick={onClick}/>
      <DropdownDivider />
      <DropdownItem label={"Item 4"} onClick={onClick}/>
    </Dropdown>
  );
}

export default SimpleDropdown;