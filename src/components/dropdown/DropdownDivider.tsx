import React from 'react';
import classNames from 'classnames'
import style from './DropdownDivider.module.scss';

type DropdownDividerProps = {
    size: 'sm' | 'lg';
}

const DropdownDivider = ({
  size,
}: DropdownDividerProps) => (
  <li
    role='separator'
    className={classNames(style.divider, style[size])}
  />
);

DropdownDivider.defaultProps = {
  size: 'sm',
};

export default DropdownDivider;
