import React from 'react';
import classNames from 'classnames'
import style from './DropdownItem.module.scss';
import Icon from '../icon/Icon';

type ItemProps = {
    children?: Array<JSX.Element> | JSX.Element;
    onClick: (label: string) => void;
    isActive: boolean;
    label: string;
}

const DropdownItem = ({
  children,
  onClick,
  isActive,
  label,
}: ItemProps) => {

  const onButtonClick = (e: React.MouseEvent<HTMLElement>) => {
    onClick(label);
    e.stopPropagation();
  }

  return (
    <li
      className={classNames(
        style.item,
        isActive && style.active,
      )}
      onClick={onButtonClick}
      tabIndex={0}
    >
      {label}
      {children && <Icon className={"dropdown-right-arrow"} />}
      {children}
    </li>
  );
};

DropdownItem.defaultProps = {
  children: null,
  onClick: (label: string) => null,
  isActive: false,
};

export default DropdownItem;
