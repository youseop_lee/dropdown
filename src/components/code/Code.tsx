import React from 'react';
import "./Code.scss";

type CodeProps = {
    children: string; 
}

const Code = ({
  children
}: CodeProps) => {
  return (
    <div className="code">
        {children}
    </div>
  );
};

export default Code;
