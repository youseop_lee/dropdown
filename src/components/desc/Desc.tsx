import './Desc.scss';
import React from 'react';

type DescProps = {
  desc: string;
}

const Desc = ({ desc }: DescProps) => {
  return (
    <div className="desc--component">{desc}</div> 
  );
}

export default Desc;
