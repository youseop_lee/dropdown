import './Button.scss';
import React from 'react';
import classNames from 'classnames'

type ButtonProps = {
  label: string;
  buttonStyle?: 'default' | 'subtle';
  isDisabled?: boolean;
  onClick: () => void;
}

const Button = ({ label, onClick, isDisabled, buttonStyle }: ButtonProps) => {
  return (
    <div className={classNames("button--component", {
        subtle: buttonStyle === 'subtle',
        disabled: isDisabled,
    })} onClick={onClick}>{label}</div> 
  );
}

export default Button;
